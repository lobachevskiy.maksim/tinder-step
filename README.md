# Tinder-Step

### Heroku - https://tinder-steps.herokuapp.com/

## DEMO DATA

1. Email: demo@gmail.com Password: demouser123
2. Email: vadim@gmail.com Password: 123321;
3. Email: roman@gmail.com Password: 0984567;
4. Email: victoria@gmail.com Password: 123456789876543;
5. Email: maksim@gmail.com Password: qwerty123;

## Participants

- Maksim Lobachevskiy - Maksim.L;
- Volodymyr Zhukivskyi - Володимир Жуківський;
- Vadim Bevziuk - Vadim Bevziuk;

## Functions distribution

- Maksim Lobachevskiy: architecture, chat, deploy
- Volodymyr Zhukivskyi - likes, users
- Vadim Bevziuk - login, filter