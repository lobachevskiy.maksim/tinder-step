package com.tinder.controller;


import javax.servlet.http.*;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {
  private TemplateEngine templateEngine;

  public LogoutServlet(TemplateEngine templateEngine) {

    this.templateEngine = templateEngine;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
    try {
      HttpSession session = req.getSession(false);
      if (session != null) {
        session.invalidate();
        resp.sendRedirect("/login");
      }
    } catch (IOException ex) {
      System.out.println(ex);
    }
  }
}
